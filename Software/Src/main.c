/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "i2c.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
const char key_name[KEYS] = {'#','9','3','6','0','8','2','5','*','7','1','4'};
volatile uint8_t read_flag, mosfet_flag;
uint8_t key_state[KEYS], key_state_prev[KEYS], buzzer = 0, zamek = 0, czysc = 0;
const char pin[4] = {'1', '4', '6', '9'};
char pin_tmp[4] = {'\0', '\0', '\0', '\0'};
uint8_t pin_state = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void key_pressed(uint8_t key);
void key_released(uint8_t key);
void read_keyboard(void);
void pin_clear(void);
void pin_check(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM17_Init();

  /* USER CODE BEGIN 2 */
  for(uint8_t i = 0; i < 4; i++)
	  HAL_GPIO_WritePin(KEYS_Port, (uint16_t) (1<<(i+3)), 1);

  HAL_TIM_Base_Start_IT(&htim17);

  for(uint8_t i = 0; i < 3; i++)
  {
	  HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, 1);
	  HAL_Delay(50);
	  HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, 0);
	  HAL_Delay(200);
  }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  if(read_flag)
	  {
		  read_keyboard();
	  	  read_flag = 0;
  	  }
	  if(mosfet_flag)
	  {
		  if(buzzer) buzzer--;
		  if(zamek) zamek--;
		  if(czysc)
		  {
			  if(czysc == 1) pin_clear();
			  czysc--;
		  }
		  HAL_GPIO_WritePin(ZAMEK_GPIO_Port, ZAMEK_Pin, zamek?1:0);
		  HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, buzzer?1:0);
		  mosfet_flag = 0;
	  }
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void key_pressed(uint8_t key)
{
	key = key_name[key];
	czysc = 100;
	if(buzzer <= 5) buzzer = 5;
	if(key != '*' && key != '#')
	{
		if(pin_state < 4)
			pin_tmp[pin_state++] = key;
		else
			pin_clear();
	}
	else if(key == '#' && pin_state == 4)
		pin_check();
	else
		pin_clear();
}

void pin_clear(void)
{
	pin_state = 0;
	for(uint8_t i = 0; i < 4; i++)
		pin_tmp[i] = '\0';
}

void pin_check(void)
{
	uint8_t pin_good = 1;
	for(uint8_t i = 0; i < 4; i++)
	{
		if(pin_tmp[i] != pin[i])
			pin_good = 0;
	}
	if(pin_good) // pin poprawny;
	{
		zamek = 100;
		buzzer = 40;
	}
	pin_clear();
}

void key_released(uint8_t key)
{
	key = key_name[key];
}

void read_keyboard(void)
{
	for(uint8_t i = 0; i < 4; i++)
	{
		HAL_GPIO_WritePin(KEYS_Port, (uint16_t) (1<<(i+3)), 0);
		for(uint8_t j = 0; j < 3; j++)
		{
			uint8_t key = i+4*j;
			key_state[key] = HAL_GPIO_ReadPin(KEYS_Port, (uint16_t) (1<<j));
			if(key_state[key] != key_state_prev[key])
			{
				if(!key_state[key]) key_pressed(key);
				else key_released(key);
				key_state_prev[key] = key_state[key];
			}
		}
		HAL_GPIO_WritePin(KEYS_Port, (uint16_t) (1<<(i+3)), 1);
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	static uint8_t cnt;
	if(htim->Instance == TIM17) // Timer co 10ms
    {
		if(++cnt%2==0) read_flag = 1;
		mosfet_flag = 1;
    }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
